package objetos.classes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class date {
	
	//Attributes
	private String alldate;
	private int day;
	private int month;
	private int year;
	
	//Format
	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	public String getAlldate() {
		return this.alldate;
	}
	public void setallAlldate(String alldate) {
		this.alldate=alldate;
	}
	
	
	public date(String date) {

		String[] splitdate = null;
		splitdate = date.split("/");
		
		this.day = Integer.parseInt(splitdate[0]);
		this.month = Integer.parseInt(splitdate[1]);
		this.year = Integer.parseInt(splitdate[2]);
		
		this.alldate=date;
	}
	
	public Calendar StringtoCalendar(String date) {
		Date Date = new Date();
		Calendar Calendar = new GregorianCalendar();
		try {
			Date = format.parse(date);
			Calendar.setTime(Date);
		} catch(ParseException e) {
			e.printStackTrace();
		}
		return Calendar;
	}
	
//	public String CalendartoString() {
//		Calendar cal = Calendar.getInstance();
//		return cal.getTime().toString();
//	}
	
	public int compareDate(date odate) {
		int compare = 0;
		Calendar date = this.StringtoCalendar(alldate);
		Calendar dater = this.StringtoCalendar(odate.toString());
		
		if(dater.before(date))
			compare = 1;//1
		else if (dater.equals(date))
			compare = 0;//0
		else if(dater.after(date))
			compare = -1;//-1

		return compare;
	}
	
	public int compareDateS() {
		int compare = 0;
		Calendar date = this.StringtoCalendar(this.alldate);
		Calendar dateSystem = Calendar.getInstance();
		
		if((date.before(dateSystem)) || (date.equals(dateSystem)))
			compare = 1;
		else if(date.after(dateSystem))
			compare = -1;
		
		return compare;
	}
	
	public boolean compareMaY(date mdate) {
		boolean valide = false;
		Calendar date = this.StringtoCalendar(alldate);
		Calendar dater = this.StringtoCalendar(mdate.toString());
		
		if(date.get(Calendar.YEAR) == dater.get(Calendar.YEAR)) {	
			if(date.get(Calendar.MONTH) == dater.get(Calendar.MONTH))
				valide = true;
		}
		
		return valide;
	}
	
	public int SubstractDateS() {
		int myyear = 0;
		int yearsys = 0;
		
		Calendar mydate = this.StringtoCalendar(this.alldate);
		Calendar datesys = Calendar.getInstance();
		
		myyear = mydate.get(Calendar.YEAR);
		yearsys = datesys.get(Calendar.YEAR);
		
		return (yearsys - myyear);
	}
	
	public int SubstractDay(date date) {
		int myyear = 0;
		int myyear2 = 0;
		
		Calendar mydate = this.StringtoCalendar(this.alldate);
		Calendar mydate2 = this.StringtoCalendar(date.toString());
		
		myyear = mydate.get(Calendar.DAY_OF_MONTH);
		myyear2 = mydate2.get(Calendar.DAY_OF_MONTH);
		
		return (myyear - myyear2);
	}
	
	public int takeMonth() {
		int month = 0;
		
		Calendar date = this.StringtoCalendar(this.alldate);
		month = date.get(Calendar.MONTH);
		
		return month;
	}
	
	public boolean v_date() {
		
		boolean validate = false;
		
		if( (this.year >= 2000) && (this.year <= thisYear())) {
			if( (this.month >= 1) && (this.month <= 12)) {
				GregorianCalendar calendar = new GregorianCalendar();
				int[] dmonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
				
				if(calendar.isLeapYear(this.year))
					dmonth[2] = 29;
				if( (this.day >= 1) && (this.day <= dmonth[this.month]))
					validate = true;
			}//end_if
		}//end_if
		
		return validate;	
	}
	
	public boolean v_date2() {
		
		boolean validate = false;
		
		if( (this.year >= thisYear()) && ( this.year <= (thisYear() + 2) ) ) {
			if(this.year == thisYear()) {
				if( (this.month >= (thisMonth() + 1)) && (this.month <= 12)) {
					GregorianCalendar calendar = new GregorianCalendar();
					int[] dmonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
					
					if(calendar.isLeapYear(this.year))
						dmonth[2] = 29;
					if( (this.day >= thisDay()) && (this.day <= dmonth[this.month]))
						validate = true;
				}//end_if
			}//end_if
			else {
				if( (this.month >= 1) && (this.month <= 12)) {
					GregorianCalendar calendar = new GregorianCalendar();
					int[] dmonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
					
					if(calendar.isLeapYear(this.year))
						dmonth[2] = 29;
					if( (this.day >= 1) && (this.day <= dmonth[this.month]))
						validate = true;
				}//end_if
			}
			
		}//end_if
		
		return validate;	
	}
	
	
	public int thisYear() {
		int year = 0;
		
		Calendar date = new GregorianCalendar();
		year = date.get(Calendar.YEAR);
		
		return year;
	}
	public int thisMonth() {
		int month = 0;
		
		Calendar date = new GregorianCalendar();
		month = date.get(Calendar.MONTH);

		return month;
	}
	public int thisDay() {
		int day = 0;
		
		Calendar date = new GregorianCalendar();
		day = date.get(Calendar.DAY_OF_MONTH);
		
		return day;
	}
	public String toString() {
		return getAlldate();
	}
	
}
