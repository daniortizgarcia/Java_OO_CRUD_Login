package objetos.modules.shoes.utils;

import objetos.utils.*;

import javax.swing.JOptionPane;

public class data_shoes {
	
	public static String requestrefc() {
		String refc = "";
		boolean carryon = false;
		
		do {
			refc = funciones.validastring("Enter reference code", "Enter reference code");
			carryon = validate.v_refc(refc);
			if(carryon == false)
				JOptionPane.showMessageDialog(null, "Enter a valid reference code");
		}while(carryon == false);
		
		return refc;
	}
	
	public static String requestemail() {
		String email = "";
		boolean carryon = false;
		
		do {
			email = funciones.validastring("Enter your email to receive offers", "Enter email");
			carryon = validate.v_email(email);
			if(carryon == false)
				JOptionPane.showMessageDialog(null, "Enter a valid email address");
		}while(carryon == false);
		
		return email;
	}
	
	public static String requesttelp() {
		String telp = "";
		boolean carryon = false;
		
		do {
			telp=funciones.validastring("Enter your telephone to contact you(+34 123456789)", "Enter telephone");
			carryon=validate.v_telp(telp);
			if(carryon == false)
				JOptionPane.showMessageDialog(null, "Enter a valid telephone");
		}while(carryon == false);
		
		return telp;
	}
}
