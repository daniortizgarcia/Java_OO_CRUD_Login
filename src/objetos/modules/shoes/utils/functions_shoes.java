package objetos.modules.shoes.utils;
import objetos.modules.shoes.classes.*;
import objetos.utils.*;
import objetos.classes.*;

public class functions_shoes {
	
	//Pedir Datos al usuario
	
	public static Flat a_flatcref() {
		Singleton.cref= data_shoes.requestrefc();
		return new Flat (Singleton.cref);
	}
	
	public static Minicleats a_mcleatscref() {
		Singleton.cref= data_shoes.requestrefc();
		return new Minicleats (Singleton.cref);
	}
	
	public static Cleats a_cleatscref() {
		Singleton.cref= data_shoes.requestrefc();
		return new Cleats (Singleton.cref);
	}
	
	public static Shoes_f requestdata(int i){		
		Shoes_f o=null;
		
		String[] t_brand = {"Adidas", "Nike", "Kelme", "Joma", "Munich"};
		String[] t_model = {"v7", "r8", "t3"};
		String[] t_material = {"Leather", "Synthetic"};
		String[] t_colour = {"Blue","Green","Pink"}; 
		String [] t_size = {"38", "39", "40", "41", "42", "43"};
//		Flat		
		String[] sole = {"Normal","Grip"};
		String[] profile = {"Tall","Medium","Small"};
//		Minicleats		
		String[] tshape_mt = {"Circular", "Linear", "Square"};
//		Cleats
		String[] t_position = {"goalkepper","defender","midfielder","forward"};
		String[] t_terrain = {"natural","natural dry","artificial"};		
		
		int size;
		String cref,brand,model,material,colour,email,telp,t_sole,t_profile,shape_mt,position,terrain;
		date darri,probe,proend;
		if(i==1)//Flat
		{
			cref = Singleton.cref;
			brand = menu.combobox(t_brand, "Select the brand you want", "Choose brand");
			model = menu.combobox(t_model, "Select the model you want", "Choose model");
			material = menu.combobox(t_material, "Select the material you want", "Choose material");
			colour = menu.combobox(t_colour, "Select the colour you want", "Choose colour");
			size = Integer.parseInt(menu.combobox(t_size, "Select the size you want", "Choose size"));
			darri = function_date_shoes.c_outlet();
			email = data_shoes.requestemail();
			telp = data_shoes.requesttelp();
			
			t_sole = menu.combobox(sole, "Select the type of sole you want", "Choose sole type");
			t_profile = menu.combobox(profile, "Select the type of profile you want", "Choose profile type");
			probe = function_date_shoes.promotion_start();
			proend = function_date_shoes.promotion_end(probe);
			o = new Flat (cref, brand, model, material, colour, size, darri, email, telp, t_sole, t_profile,probe,proend);
		}
		else if(i==2)//Minicleats
		{
			cref = Singleton.cref;
			brand = menu.combobox(t_brand, "Select the brand you want", "Choose brand");
			model = menu.combobox(t_model, "Select the model you want", "Choose model");
			material = menu.combobox(t_material, "Select the material you want", "Choose material");
			colour = menu.combobox(t_colour, "Select the colour you want", "Choose colour");
			size = Integer.parseInt(menu.combobox(t_size, "Select the size you want", "Choose size"));
			darri = function_date_shoes.c_outlet();
			email = data_shoes.requestemail();
			telp = data_shoes.requesttelp();
			
			shape_mt = menu.combobox(tshape_mt, "Select the shape of minicleats","Choose shape minicleats");			
			o = new Minicleats (cref, brand, model, material, colour, size, darri, email, telp, shape_mt);
		}
		else if(i==3)//Cleats
		{	
			cref = Singleton.cref;
			brand = menu.combobox(t_brand, "Select the brand you want", "Choose brand");
			model = menu.combobox(t_model, "Select the model you want", "Choose model");
			material = menu.combobox(t_material, "Select the material you want", "Choose material");
			colour = menu.combobox(t_colour, "Select the colour you want", "Choose colour");
			size = Integer.parseInt(menu.combobox(t_size, "Select the size you want", "Choose size"));
			darri = function_date_shoes.c_outlet();
			email = data_shoes.requestemail();
			telp = data_shoes.requesttelp();
			
			position = menu.combobox(t_position, "Select your position in the field", "Choose position");
			terrain = menu.combobox(t_terrain, "Select the terrain you play", "Choose terrain");
			o = new Cleats (cref, brand, model, material, colour, size, darri, email, telp, position, terrain);
		}
		return o;
	}
	
	
	//Cambiar datos
	public static void changedata(Shoes_f o){
		
		String[] t_brand = {"Adidas", "Nike", "Kelme", "Joma", "Munich"};
		String[] t_model = {"v7", "r8", "t3"};
		String[] t_material = {"Leather", "Synthetic"};
		String[] t_colour = {"Blue","Green","Pink"}; 
		String [] t_size = {"38", "39", "40", "41", "42", "43"};
//		Flat		
		String[] sole = {"Normal","Grip"};
		String[] profile = {"Tall","Medium","Small"};
//		Minicleats		
		String[] tshape_mt = {"Circular", "Linear", "Square"};
//		Cleats
		String[] t_position = {"goalkepper","defender","midfielder","forward"};
		String[] t_terrain = {"natural","natural dry","artificial"};	
		
		if(o instanceof Flat){//Flat
			String[] type = {"Reference code", "Brand", "Model", "Material", "Colour", "Size", "Date Arrive", "Email", "Telephone", "Type sole", "Type profile", " date start", "date end"}; 
			int choose = 0;
			date savepro = null;
			
			choose = menu.validaboton(type, "Select one option", "Select option");
			switch(choose){
				
				case 0:
					((Flat)o).setCref(data_shoes.requestrefc());
					break;
				case 1:
					((Flat)o).setBrand(menu.combobox(t_brand, "Select the brand you want", "Choose brand"));
					break;
				case 2:
					((Flat)o).setModel(menu.combobox(t_model, "Select the model you want", "Choose model"));
					break;
				case 3: 
					((Flat)o).setMaterial(menu.combobox(t_material, "Select the material you want", "Choose material"));
					break;
				case 4: 
					((Flat)o).setColour(menu.combobox(t_colour, "Select the colour you want", "Choose colour"));
					break;
				case 5: 
					((Flat)o).setSize(Integer.parseInt(menu.combobox(t_size, "Select the size you want", "Choose size")));
					break;
				case 6:
					((Flat)o).setDarri(function_date_shoes.c_outlet());
					break;
				case 7:
					((Flat)o).setEmail(data_shoes.requestemail());
					break;
				case 8:
					((Flat)o).setTelp(data_shoes.requesttelp());
					break;
				case 9: 
					((Flat)o).setT_sole(menu.combobox(sole, "Select the type of sole you want", "Choose sole type"));
					break;
				case 10: 
					((Flat)o).setT_profile(menu.combobox(profile, "Select the type of profile you want", "Choose profile type"));
					break;
				case 11:
					((Flat)o).setProbe(function_date_shoes.v_dateps(((Flat)o)));
					break;
				case 12:
					savepro = ((Flat)o).getProbe();
					((Flat)o).setProend(function_date_shoes.promotion_end(savepro));
					break;
			}
		}
		if(o instanceof Minicleats){//Minicleats
			String[] type = {"Reference code", "Brand", "Model", "Material", "Colour", "Size", "Date Arrive", "Email", "Telephone", "Forma Minicleats"}; 
			int choose = 0;
			
			choose = menu.validaboton(type, "Select onr option", "Select option");
			switch(choose){
				case 0:
					((Minicleats)o).setCref(data_shoes.requestrefc());
					break;
				case 1:
					((Minicleats)o).setBrand(menu.combobox(t_brand, "Select the brand you want", "Choose brand"));
					break;
				case 2:
					((Minicleats)o).setModel(menu.combobox(t_model, "Select the model you want", "Choose model"));
					break;
				case 3: 
					((Minicleats)o).setMaterial(menu.combobox(t_material, "Select the material you want", "Choose material"));
					break;
				case 4: 
					((Minicleats)o).setColour(menu.combobox(t_colour, "Select the colour you want", "Choose colour"));
					break;
				case 5: 
					((Minicleats)o).setSize(Integer.parseInt(menu.combobox(t_size, "Select the size you want", "Choose size")));
					break;
				case 6:
					((Minicleats)o).setDarri(function_date_shoes.c_outlet());
					break;
				case 7:
					((Minicleats)o).setEmail(data_shoes.requestemail());
					break;
				case 8:
					((Minicleats)o).setTelp(data_shoes.requesttelp());
					break;
				case 9: 
					((Minicleats)o).setShape_mt(menu.combobox(tshape_mt, "Select the shape of minicleats","Choose shape minicleats"));
					break;
			}
		}
		if(o instanceof Cleats){
			String[] type = {"Reference code", "Brand", "Model", "Material", "Colour", "Size", "Date Arrive", "Email", "Telephone", "position", "terrain"}; 
			int choose = 0;
			
			choose = menu.validaboton(type, "Select onr option", "Select option");
			switch(choose){
				case 0:
					((Cleats)o).setCref(data_shoes.requestrefc());
					break;
				case 1:
					((Cleats)o).setBrand(menu.combobox(t_brand, "Select the brand you want", "Choose brand"));
					break;
				case 2:
					((Cleats)o).setModel(menu.combobox(t_model, "Select the model you want", "Choose model"));
					break;
				case 3: 
					((Cleats)o).setMaterial(menu.combobox(t_material, "Select the material you want", "Choose material"));
					break;
				case 4: 
					((Cleats)o).setColour(menu.combobox(t_colour, "Select the colour you want", "Choose colour"));
					break;
				case 5: 
					((Cleats)o).setSize(Integer.parseInt(menu.combobox(t_size, "Select the size you want", "Choose size")));
					break;
				case 6:
					((Cleats)o).setDarri(function_date_shoes.c_outlet());
					break;
				case 7:
					((Flat)o).setEmail(data_shoes.requestemail());
					break;
				case 8:
					((Cleats)o).setTelp(data_shoes.requesttelp());
					break;
				case 9: 
					((Cleats)o).setPosition(menu.combobox(t_position, "Select your position in the field", "Choose position"));
					break;
				case 10: 
					((Cleats)o).setTerrain(menu.combobox(t_terrain, "Select the terrain you play", "Choose terrain"));
					break;
			}
		}
	}
	
	public static String readdata(Shoes_f o){
		String s = "";
		
		if(o instanceof Flat){//Flat
			s=o.toString();
		}
		if(o instanceof Minicleats){//Minicleats
			s=o.toString();
		}
		if(o instanceof Cleats){
			s=o.toString();
		}
		return s;
	}
	
	public static Shoes_f deletedata(Shoes_f o){
		if(o instanceof Flat){//Flat
			o=null;
		}
		if(o instanceof Minicleats){//Minicleats
			o=null;
		}
		if(o instanceof Cleats){
			o=null;
		}
		return o;
	}

}
