package objetos.modules.shoes.utils.CRUD;

import javax.swing.JOptionPane;

import objetos.modules.shoes.classes.*;

public class function_delete {
	
	public static void d_flat() {
		if(Singleton.shoesflat.isEmpty()) 
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			Singleton.shoesflat.remove(function_find.f_flat(function_find.IDflat()));
			JOptionPane.showMessageDialog(null, "Deleted");
		}//end_else
	}
	
	
	public static void d_mcleats() {
		if(Singleton.shoesminicleats.isEmpty()) 
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			Singleton.shoesminicleats.remove(function_find.f_mcleats(function_find.IDminicleats()));
			JOptionPane.showMessageDialog(null, "Deleted");
		}//end_else
		
	}
	
	public static void d_cleats() {
		if(Singleton.shoescleats.isEmpty()) 
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			Singleton.shoescleats.remove(function_find.f_cleats(function_find.IDcleats()));
			JOptionPane.showMessageDialog(null, "Deleted");
		}//end_else
		
	}
	
}
