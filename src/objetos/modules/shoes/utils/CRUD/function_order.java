package objetos.modules.shoes.utils.CRUD;

import javax.swing.JOptionPane;
import java.util.Collections;
import objetos.modules.shoes.classes.*;
import objetos.modules.shoes.classes.order.*;
import objetos.utils.*;

public class function_order {

	public static void o_flat() {
		String[] type = {"Brand", "Date arrive", "Size"};
		int choose = 0;
		
		if(Singleton.shoesflat.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			choose = menu.validaboton(type, "Select the option you prefer", "Select option");
			switch(choose) {
				case 0:
					Collections.sort(Singleton.shoesflat, new orderbrand());
					break;
				case 1:
					Collections.sort(Singleton.shoesflat, new orderdarri());
					break;
				case 2:
					Collections.sort(Singleton.shoesflat, new ordersize());
					break;
			}
		}
	}

	public static void o_mcleats() {
		String[] type = {"Brand", "Date arrive", "Size"};
		int choose = 0;
		
		if(Singleton.shoesminicleats.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			choose = menu.validaboton(type, "Select the option you prefer", "Select option");
			switch(choose) {
				case 0:
					Collections.sort(Singleton.shoesminicleats, new orderbrand());
					break;
				case 1:
					Collections.sort(Singleton.shoesminicleats, new orderdarri());
					break;
				case 2:
					Collections.sort(Singleton.shoesminicleats, new ordersize());
					break;
			}
		}
	}
	
	public static void o_cleats() {
		String[] type = {"Brand", "Date arrive", "Size"};
		int choose = 0;
		
		if(Singleton.shoescleats.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			choose = menu.validaboton(type, "Select the option you prefer", "Select option");
			switch(choose) {
				case 0:
					Collections.sort(Singleton.shoescleats, new orderbrand());
					break;
				case 1:
					Collections.sort(Singleton.shoescleats, new orderdarri());
					break;
				case 2:
					Collections.sort(Singleton.shoescleats, new ordersize());
					break;
			}
		}
	}
	
}
