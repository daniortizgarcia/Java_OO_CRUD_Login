package objetos.modules.shoes.utils.CRUD;

import javax.swing.JOptionPane;

import objetos.modules.shoes.classes.*;
import objetos.modules.shoes.utils.*;

public class function_create {

	public static void c_flat(Flat f) {
		int position = 0;
		
		f = functions_shoes.a_flatcref();
		position = function_find.f_flat(f);
		
		if(position != -1) 
			JOptionPane.showMessageDialog(null, "The reference code is already created");
		else {
			f = (Flat)functions_shoes.requestdata(1);
			Singleton.shoesflat.add(f);
		}
		
	}
	
	public static void c_mcleats(Minicleats m) {
		int position = 0;
		
		m = functions_shoes.a_mcleatscref();
		position = function_find.f_mcleats(m);
		
		if(position != -1)
			JOptionPane.showMessageDialog(null, "The reference code is already created");
		else {
			m = (Minicleats)functions_shoes.requestdata(2);
			Singleton.shoesminicleats.add(m);
		}
	}
	
	public static void c_cleats(Cleats c) {
		int position = 0;
		
		c = functions_shoes.a_cleatscref();
		position = function_find.f_cleats(c);
		
		if(position != -1)
			JOptionPane.showMessageDialog(null, "The reference code is already created");
		else {
			c = (Cleats)functions_shoes.requestdata(3);
			Singleton.shoescleats.add(c);
		}
		
	}
}
