package objetos.modules.shoes.utils.CRUD;

import objetos.classes.*;
import objetos.modules.shoes.classes.*;

public class dummies {
//	Shoes_f
	private static String[] t_brand = {"Adidas", "Nike", "Kelme", "Joma", "Munich", "Mizuno", "Penalty", "Kipsta" , "Puma" , "Umbro", "Lotto"};
	private static String[] t_model = {	"Nemeziz 17", "Ace 17", "Predator Precision", "Hypervenom", "Magista", "Mercurial", "Feline", "Subito", "Precision", 
										"Propulsion", "Sala max", "Dribling", "Mundial", "Continental", "Gresca", "Monarcida", "Sala premium", "Sala clasic",
										"Commander", "Matis", "Max 400", "CLR300", "Agility", "CLR 500", "Evopower", "EvoSpeed", "Evotouch",
										"Eternal", "Medusa", "clasic", "Zhero gravity 200", "Zhero gravity 700", "Spider"};
	private static String[] t_material = {"Leather", "Synthetic"};
	private static String[] t_colour = {"Blue","Green","Pink", "Yellow", "White", "Purple", "Black", "Red", "Orange", "Brown"}; 
	private static date[] darrive = {new date("10/10/2017"), new date("15/08/2012"), new date("01/11/2014"), new date("17/08/2015"), new date("25/04/2016"), new date("02/11/2017") };
	private static String[] t_email = {"dani@gmail.com", "pepe@gmail.com", "manolo@gmail.com", "esteban@gmail.com", "aleja@gmail.com", "adri@gmail.com"};
//	Flat		
	private static String[] t_sole = {"Normal","Grip"};
	private static String[] t_profile = {"Tall","Medium","Small"};
	private static date[] start_p = {new date("10/10/2018"), new date("10/12/2017"), new date("10/06/2018"), new date("01/03/2019"), new date("10/07/2019"), new date("08/04/2018") };
	private static date[] end_p = {new date("15/10/2018"), new date("20/12/2017"), new date("19/06/2018"), new date("05/03/2019"), new date("20/07/2019"), new date("25/04/2018") };
//	Minicleats		
	private static String[] tshape_mt = {"Circular", "Linear", "Square"};
//	Cleats
	private static String[] t_position = {"goalkepper","defender","midfielder","forward"};
	private static String[] t_terrain = {"natural","natural dry","artificial"};

		
	public static String c_ref() {
		String cad = "";
		String[] letras = {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","ñ","z","x","c","v","b","n","m","ç"};
		
		while(cad.length() < 5) {
			cad = cad + ( (int) (Math.random() * 10) );
		}
		
		return cad + "-" + letras[(int) (Math.random() * 27)];
	}
	
	public static String brand() {
		return t_brand[(int) (Math.random() * 11)];
	}
	
	public static String model(String brand) {
		String model = "";
		switch(brand) {
			case "Adidas":
				model = t_model[(int) (Math.random() * 3)];//Nemeziz 17, Ace 17, Predator Precision.
				break;
			case "Nike":
				model = t_model[(int) ((Math.random() * 3) + 3)];//Hypervenom, Magista, Mercurial
				break;
			case "Kelme":
				model = t_model[(int) ((Math.random() * 3) + 6)];//Feline, Subito, Precision
				break;
			case "Joma":
				model = t_model[(int) ((Math.random() * 3) + 9)];//Propulsion, Sala max, Dribling
				break;
			case "Munich":
				model = t_model[(int) ((Math.random() * 3) + 12)];//Mundial, Continental, Gresca
				break;
			case "Mizuno":
				model = t_model[(int) ((Math.random() * 3) + 15)];//Monarcida, Sala premium, Sala clasic
				break;
			case "Penalty":
				model = t_model[(int) ((Math.random() * 3) + 18)];//Commander, Matis, Max 400
				break;
			case "Kipsta":
				model = t_model[(int) ((Math.random() * 3) + 21)];//CLR300, Agility, CLR 500
				break;
			case "Puma":
				model = t_model[(int) ((Math.random() * 3) + 24)];//Evopower, Evospeed, Evotouch
				break;
			case "Umbro":
				model = t_model[(int) ((Math.random() * 3) + 27)];//Eternal, Medusa, Clasic
				break;
			case "Lotto":
				model = t_model[(int) ((Math.random() * 3) + 30)];//Zhero gravity 200, Zhero gravity 700, Spider
				break;
		}
		return model;
	}
	public static String material() {
		return t_material[(int) (Math.random() * 2)];
	}
	
	public static String colour() {
		return t_colour[(int) (Math.random() * 10)];
	}
	
	public static int size() {
		return (int) ( (Math.random() * 15) + 30 );
	}
	
	public static date darri() {
		return darrive[(int) (Math.random() * 6)];
	}
	public static String email() {
		return t_email[(int) (Math.random() * 6)];
	}
	public static String telp() {
		String cad = "";
		
		while(cad.length() < 8) {
			cad = cad + ( (int) (Math.random() * 9));
		}
		
		return "+34 6" + cad;
	}
	public static String sole() {
		return t_sole[(int) (Math.random() * 2)];
	}
	public static String profile() {
		return t_profile[(int) (Math.random() * 3)];
	}
	public static date pros() {
		return start_p[(int) (Math.random() * 6)];
	}
	public static date proe() {
		return end_p[(int) (Math.random() * 6)];
	}
	public static String shape_mt() {
		return tshape_mt[(int) (Math.random() * 3)];
	}
	public static String position() {
		return t_position[(int) (Math.random() * 4)];
	}
	public static String terrain() {
		return t_terrain[(int) (Math.random() * 3)];
	}
	public static void load_dummies() {
		Flat f = null;
		Minicleats m = null;
		Cleats c = null;
		String brand = "";
		
		for(int i=0; i < 5; i++) {
			brand = brand();
			f = new Flat(c_ref(),brand,model(brand), material(), colour(), size(), darri(), email(), telp(), sole(), profile(), pros(), proe());
			brand = brand();
			m = new Minicleats(c_ref(), brand,model(brand), material(), colour(), size(), darri(), email(), telp(), shape_mt());
			brand = brand();
			c = new Cleats(c_ref(), brand,model(brand), material(), colour(), size(), darri(), email(), telp(), position(), terrain());
			Singleton.shoesflat.add(f);
			Singleton.shoesminicleats.add(m);
			Singleton.shoescleats.add(c);
		}
		
	}
	
	

}
