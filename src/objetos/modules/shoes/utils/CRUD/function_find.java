package objetos.modules.shoes.utils.CRUD;

import objetos.modules.shoes.classes.*;
import objetos.utils.menu;

public class function_find {

	public static int f_flat(Flat f) {
		for (int i = 0; i <= (Singleton.shoesflat.size() - 1); i++) {
			if (Singleton.shoesflat.get(i).equals(f))
				return i;
		}
		return -1;
	}

	public static int f_mcleats(Minicleats m) {
		for (int i = 0; i <= (Singleton.shoesminicleats.size() - 1); i++) {
			if (Singleton.shoesminicleats.get(i).equals(m))
				return i;
		}
		return -1;
	}

	public static int f_cleats(Cleats c) {
		for (int i = 0; i <= (Singleton.shoescleats.size() - 1); i++) {
			if (Singleton.shoescleats.get(i).equals(c))
				return i;
		}
		return -1;
	}

	public static String[] vector_flat() {
		Flat f = null;
		String s = "";
		String[] user = new String[Singleton.shoesflat.size()];

		for (int i = 0; i < Singleton.shoesflat.size(); i++) {
			f = (Flat) Singleton.shoesflat.get(i);
			s = f.getCref() + "//" + f.getBrand() + "//" + f.getSize();
			user[i] = s;
		}

		return user;
	}

	public static String[] vector_mcleats() {
		Minicleats m = null;
		String s = "";
		String[] user = new String[Singleton.shoesminicleats.size()];

		for (int i = 0; i < Singleton.shoesminicleats.size(); i++) {
			m = (Minicleats) Singleton.shoesminicleats.get(i);
			s = m.getCref() + "//" + m.getBrand() + "//" + m.getSize();
			user[i] = s;
		}
		return user;
	}

	public static String[] vector_cleats() {
		Cleats c = null;
		String s = "";
		String[] user = new String[Singleton.shoescleats.size()];

		for (int i = 0; i < Singleton.shoescleats.size(); i++) {
			c = (Cleats) Singleton.shoescleats.get(i);
			s = c.getCref() + "//" + c.getBrand() + "//" + c.getSize();
			user[i] = s;
		}

		return user;
	}

	public static Flat IDflat() {
		Flat f = null;
		String id = "";
		String search = menu.combobox(vector_flat(), "Choose one option", "Choose option");
			for (int i = 0; i < 7; i++) {
				id = id + search.charAt(i);
			}
			f = new Flat(id);
		return f;
	}
	
	public static Minicleats IDminicleats() {
		Minicleats m = null;
		String id = "";
		String search = menu.combobox(vector_mcleats(), "Choose one option", "Choose option");
			for (int i = 0; i < 7; i++) {
				id = id + search.charAt(i);
			}
			m = new Minicleats(id);
		return m;
	}
	
	public static Cleats IDcleats() {
		Cleats c = null;
		String id = "";
		String search = menu.combobox(vector_cleats(), "Choose one option", "Choose option");
			for (int i = 0; i < 7; i++) {
				id = id + search.charAt(i);
			}
			c = new Cleats(id);
		return c;
	}

}
