package objetos.modules.shoes.utils.CRUD;

import javax.swing.JOptionPane;

import objetos.modules.shoes.classes.*;
import objetos.utils.*;

public class function_read {

	
	
	public static void r_flat() {
		int choice = 0;
		String[] type = {"All", "One"}; 
				
		if(Singleton.shoesflat.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			choice = menu.validaboton(type, "Select the option you prefer", "Select option");
			switch(choice) {
				case 0:
					for(int i = 0;i < Singleton.shoesflat.size(); i++) {
						JOptionPane.showMessageDialog(null, (Singleton.shoesflat.get(i)).toString());
					}//end_for
					break;
				case 1:
						JOptionPane.showMessageDialog(null, Singleton.shoesflat.get(function_find.f_flat(function_find.IDflat())).toString());
					break;
			}//end_switch
		}//end_else
	}//end_rflat
	
	public static void r_mcleats(){
		int choice = 0;
		String[] type = {"All", "One"};
		
		if(Singleton.shoesminicleats.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			choice = menu.validaboton(type, "Select the option you prefer", "Select option");
			switch(choice) {
			case 0:
				for(int i = 0; i < Singleton.shoesminicleats.size(); i++) {
					JOptionPane.showMessageDialog(null, (Singleton.shoesminicleats.get(i)).toString());
				}//end_for
				break;
			case 1:
				JOptionPane.showMessageDialog(null, Singleton.shoesminicleats.get(function_find.f_mcleats(function_find.IDminicleats())).toString());
				break;
			}
		}//end_else
	}//end_rmcleats
	
	public static void r_cleats() {
		int choice = 0;
		String[] type = {"All", "One"}; 
				
		if(Singleton.shoescleats.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			choice = menu.validaboton(type, "Select the option you prefer", "Select option");
			switch(choice) {
				case 0:
					for(int i = 0;i < Singleton.shoescleats.size(); i++) {
						JOptionPane.showMessageDialog(null, (Singleton.shoescleats.get(i)).toString());
					}//end_for
					break;
				case 1:
					JOptionPane.showMessageDialog(null, Singleton.shoescleats.get(function_find.f_cleats(function_find.IDcleats())).toString());
					break;
			}//end_switch
		}//end_else
	}
}
