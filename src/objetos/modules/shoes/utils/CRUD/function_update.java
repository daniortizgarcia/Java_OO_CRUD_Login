package objetos.modules.shoes.utils.CRUD;

import javax.swing.JOptionPane;

import objetos.modules.shoes.classes.*;
import objetos.modules.shoes.utils.functions_shoes;

public class function_update {

	public static void u_flat(Flat f) {
		int position = 0;
		
		if(Singleton.shoesflat.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			position = function_find.f_flat(function_find.IDflat());
			f = Singleton.shoesflat.get(position);
			functions_shoes.changedata(f);
			Singleton.shoesflat.set(position, f);
		}//end_else
	}
	
	public static void u_mcleats(Minicleats m) {
		int position = 0;
		
		if(Singleton.shoesminicleats.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			position = function_find.f_mcleats(function_find.IDminicleats());
			m = Singleton.shoesminicleats.get(position);
			functions_shoes.changedata(m);
			Singleton.shoesminicleats.set(position, m);
		}
	}

	public static void u_cleats(Cleats c) {
		int position = 0;
		
		if(Singleton.shoescleats.isEmpty())
			JOptionPane.showMessageDialog(null, "Is empty");
		else {
			position = function_find.f_cleats(function_find.IDcleats());
			c = Singleton.shoescleats.get(position);
			functions_shoes.changedata(c);
			Singleton.shoescleats.set(position, c);
		}//end_if
	}
	
}
