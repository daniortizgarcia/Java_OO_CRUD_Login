package objetos.modules.shoes.utils;

import javax.swing.JOptionPane;
import objetos.classes.*;
import objetos.utils.funciones;
import objetos.utils.validate;
import objetos.modules.shoes.classes.*;

public class function_date_shoes {
	public static date c_outlet() {
		String date = "";
		boolean result = false;
		date d = null;
		int compare = 0;
		
		do {
			date = funciones.validastring("Enter a date (01/02/2017)", "Date");//Enter date
			result = validate.v_date(date);//Validate format date
			if( result == false) 
				JOptionPane.showMessageDialog(null, "Enter a valid format date");
			else {
				d = new date(date);//creating the object
				result = d.v_date();//Valida date
				if(result == false)
					JOptionPane.showMessageDialog(null, "Enter a valid date, You can't enter a date greater than the current date");
				else {
					compare = d.compareDateS();//Compare date
					if(compare == 1)
						result = true;//Substract date
					else if(compare == -1) {
						result = false;
						JOptionPane.showMessageDialog(null, "You can't enter a date greater than the current date");
					}//end_else
				}//end_else
			}//end_else
		}while(result == false);//end_while
		return d;
	}
	
	public static date promotion_start(){
		String date = "";
		date e = null;
		int compare = 0;
		boolean result = false;
		
		do {
			date = funciones.validastring("Enter a date (01/02/2017)", "Date");
			result = validate.v_date(date);
			if(result == false)
				JOptionPane.showMessageDialog(null, "Enter a valid format date");
			else {
				e = new date(date);
				result = e.v_date2();
				if(result == false)
					JOptionPane.showMessageDialog(null, "Enter a valid date, You can't enter a date that is less than the current date");
				else {
					compare = e.compareDateS();
					if(compare == -1) {
						result = true;//TRUE
					}
					if(compare == 1)
						JOptionPane.showMessageDialog(null, "You can't enter a date that is less than the current date");
				}//end_else
			}//end_else
		}while(result == false);//end_while
		
		return e;
	}
	
	public static date promotion_end(date odate){
		String date = "";
		date e = null;
		int compare = 0;
		boolean result = false;
		int cday = 0;
		int month = 0;
		
		do {
			date = funciones.validastring("Enter a date (01/02/2017)", "Date");
			result = validate.v_date(date);
			if(result == false)
				JOptionPane.showMessageDialog(null, "Enter a valid format date");
			else {
				e = new date(date);
				result = e.v_date2();
				if(result == false)
					JOptionPane.showMessageDialog(null, "Enter a valid date, You can't enter a date that is less than the current date");
				else {
					result = e.compareMaY(odate);
					if(result == false)
						JOptionPane.showMessageDialog(null, "The month and year must coincide.");
					else {
						compare = e.compareDate(odate);
						if(compare == 1) {
							month =  e.takeMonth();
							if( ( month >= 8 ) && (month <= 10) ) {
								cday = e.SubstractDay(odate);
								if((cday >= 1) && (cday <= 5)) {
									result = true;
								}
								else {
									JOptionPane.showMessageDialog(null, "The duration of the promotion must be between 1 and 5 days.");
									result = false;
								}
							}//end_if
							else if ( (month >= 6) && (month <= 7)) {
								cday = e.SubstractDay(odate);
								if((cday >= 7) && (cday <= 14)) {
									result = true;
								}//end_if
								else {
									result = false;
									JOptionPane.showMessageDialog(null, "The duration of the promotion must be between 7 and 14 days.");
								}//end_else
							}//end_elseif
						}//end_if
						else if(compare == 0) {
							JOptionPane.showMessageDialog(null, "The promotion must last at least one day.");
							result = false;
						}
						else if(compare == -1) {
							JOptionPane.showMessageDialog(null, "The start date of the promotion is longer than the end date.");
							result = false;
						}
					}
				}
			}//end_else	
			
		}while(result == false);
		
		return e;
	}
	
	public static date v_dateps(
			Flat f) {
		date s  = null; date e = ((Flat)f).getProend();
		boolean result = false;
		int compare = 0;
		int cday = 0;
		int month = 0;
		
		do {
			s = function_date_shoes.promotion_start();
			result = e.compareMaY(s);
			if(result == false)
				JOptionPane.showMessageDialog(null, "The month and year must coincide.");
			else {
				compare = e.compareDate(s);
				if(compare == 1) {
					
					month =  e.takeMonth();
					if( ( month >= 8 ) && (month <= 10) ) {
						cday = e.SubstractDay(s);
						if((cday >= 1) && (cday <= 5)) {
							result = true;
						}
						else {
							JOptionPane.showMessageDialog(null, "The duration of the promotion must be between 1 and 5 days.");
							result = false;
						}
					}//end_if
					else if ( (month >= 6) && (month <= 7)) {
						cday = e.SubstractDay(s);
						if((cday >= 7) && (cday <= 14)) {
							result = true;
						}//end_if
						else {
							result = false;
							JOptionPane.showMessageDialog(null, "The duration of the promotion must be between 7 and 14 days.");
						}//end_else
					}//end_elseif
					
				}//end_if
				else if(compare == 0) {
					JOptionPane.showMessageDialog(null, "The promotion must last at least one day.");
					result = false;
				}
				else if(compare == -1) {
					JOptionPane.showMessageDialog(null, "The start date of the promotion is longer than the end date.");
					result = false;
				}
			}
			
		}while(result == false);
		
		return s;
	}
}
