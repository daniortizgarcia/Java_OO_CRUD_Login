package objetos.modules.shoes.classes;

import objetos.classes.*;

public class Cleats extends Shoes_f {

	private String position;
	private String shape_t;
	private String terrain;
	private String material_t;
	
	public Cleats (String cref, String brand, String model, String material, String colour, int size, date darri, String email, String telp, String position, String terrain) {
		super (cref, brand, model, material, colour, size, darri, email, telp);
		this.position=position;
		this.terrain=terrain;
		this.Choiceshape();
		this.Choicematerial();
	}
	
	public Cleats() {
		super();
	}
	public Cleats(String cref) {
		super(cref);
	}
	
	//getter

	public String getPosition() {
		return this.position;
	}
	public String getTerrain() {
		return this.terrain;
	}
	
	//setter
	
	public void setPosition(String position) {
		this.position=position;
		this.Choiceshape();
	}
	public void setTerrain(String terrain) {
		this.terrain=terrain;
		this.Choicematerial();
	}
	
	//Calculate
	
	public void Choiceshape() {
		if( this.position == "goalkepper" || this.position == "defender") 
			this.shape_t="ciruclar";
		else if ( position == "midfielder" )
			this.shape_t="Square";
		else if (position == "forward")
			this.shape_t="Linear";
	}
	
	public void Choicematerial() {
		if(this.terrain == "natural")
			this.material_t="Aluminium";
		else if (terrain == "natural dry" || terrain == "artificial")
			this.material_t="Rubber";
	}
	
	
	public String toString() {
		return 	"Reference code: " + this.getCref() + "\nBrand: " + this.getBrand() + "\nModel: " + this.getModel() + "\nMaterial: " + this.getMaterial() + "\nColour: " + this.getColour() + "\nSize: " + this.getSize() + "\nYour email: " + this.getEmail() + "\nYour telephone: " + this.getTelp() + "\nOutlet: " + this.outlet + 
				"\nCleats depending the position: " + this.shape_t + "\nMaterial depending on the terrain: " + this.material_t + "\n";
	}
	
	public String toString(String cref) {
		String cad = "";
		
		cad=cad + (super.getCref() + "\n");
		
		return cad;
	}
	
}
