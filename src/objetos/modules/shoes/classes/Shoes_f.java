package objetos.modules.shoes.classes;

import objetos.classes.*;

public abstract class Shoes_f implements Comparable<Shoes_f>{
	
	//Atributos
	private String cref;
	private String brand;
	private String model;
	private String material;
	private String colour;
	private int size;
	private String email;
	private String telp;
	
	private date darri;
	protected String outlet;
	
	
	//Metodos 
	
	public Shoes_f(String cref, String brand, String model, String material, String colour, int size, date darri, String email, String telp) {
		this.cref = cref;
		this.brand=brand;
		this.model=model;
		this.material=material;
		this.colour=colour;
		this.size=size;
		this.darri=darri;
		this.email=email;
		this.telp=telp;
		this.calculateoutlet();
	}
	
	public Shoes_f() {
	}
	
	public Shoes_f(String cref) {
		this.cref = cref;
	}
	
	//getter
	
	public String getCref() {
		return this.cref;
	}
	
	public String getBrand () {
		return this.brand;
	}
	
	public String getModel () {
		return this.model;
	}
	
	public String getMaterial () {
		return this.material;
	}
	public String getColour () {
		return this.colour;
	}
	public int getSize () {
		return this.size;
	}
	public date getDarri() {
		return this.darri;
	}
	public String getEmail() {
		return this.email;
	}
	public String getTelp() {
		return this.telp;
	}
	
	//setter
	public void setCref(String cref) {
		this.cref = cref;
	}
	public void setBrand  (String brand) {
		this.brand=brand;
	}
	
	public void setModel  (String model) {
		this.model=model;
	}
	
	public void setMaterial  (String material) {
		this.material=material;
	}
	public void setColour (String colour) {
		this.colour=colour;
	}
	public void setSize (int size) {
		this.size=size;
	}
	
	public void setDarri (date darri) {
		this.darri = darri;
		this.calculateoutlet();
	}
	
	public void setEmail(String email) {
		this.email= email;
	}
	
	public void setTelp(String telp) {
		this.telp=telp;
	}
	
	public void calculateoutlet() {
		if(darri.SubstractDateS() > 2)
			this.outlet="Yes";
		else
			this.outlet="No";
	}
	
	public int compareTo(Shoes_f param) {
		if(this.getCref().compareTo(param.getCref())>0)
			return 1;
		if(this.getCref().compareTo(param.getCref())<0)
			return -1;
		return 0;
	}
	
	public boolean equals(Object param) {
		return getCref().equals(((Shoes_f)param).getCref());
	}
	
	//toString
	public abstract String toString();
	
}

