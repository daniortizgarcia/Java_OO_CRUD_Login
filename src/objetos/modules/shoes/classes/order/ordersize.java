package objetos.modules.shoes.classes.order;

import java.util.Comparator;
import objetos.modules.shoes.classes.*;

public class ordersize implements Comparator <Shoes_f>{
	
	public int compare (Shoes_f foption, Shoes_f soption) {
		if(foption.getSize()>soption.getSize()) 
			return 1;
		if(foption.getSize()<soption.getSize())
			return -1;
		
		return 0;
	}
	
}
