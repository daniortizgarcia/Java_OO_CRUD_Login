package objetos.modules.shoes.classes.order;

import java.util.Comparator;
import objetos.modules.shoes.classes.*;

public class orderbrand implements Comparator <Shoes_f> {

	public int compare (Shoes_f foption, Shoes_f soption) {
		if(foption.getBrand().compareTo(soption.getBrand())>0)
			return 1;
		if(foption.getBrand().compareTo(soption.getBrand())<0)
			return -1;
		return 0;
	}
	
}
