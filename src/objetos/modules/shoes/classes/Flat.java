package objetos.modules.shoes.classes;

import objetos.classes.*;

public class Flat extends Shoes_f {
	
	private String t_sole;
	private String t_profile;
	private date probe;
	private date proend;
	private int coupon;
	
	public Flat (String cref, String brand, String model, String material, String colour, int size, date darri, String email, String telp, String t_sole, String t_profile, date probe, date proend) {
		super (cref, brand, model, material, colour, size, darri, email, telp);
		this.t_sole=t_sole;
		this.t_profile=t_profile;
		this.probe = probe;
		this.proend = proend;
		this.Calcdiscount(probe);
	}
	
	
	public Flat() {
		super();
	}
	public Flat(String cref) {
		super(cref);
	}
	
	//getter

	public String getT_Sole() {
		return this.t_sole;
	}
	public String getT_Profile() {
		return this.t_profile;
	}
	public date getProbe() {
		return this.probe;
	}
	public date getProend() {
		return this.proend;
	}
	public int getCoupon(){
		return this.coupon;
	}
	//setter
	
	public void setT_sole(String t_sole) {
		this.t_sole=t_sole;
	}
	public void setT_profile(String t_profile) {
		this.t_profile=t_profile;
	}
	public void setProbe(date probe) {
		this.probe = probe;
		System.out.println(this.probe);
	}
	public void setProend(date proend) {
		this.proend = proend;
		System.out.println(this.proend);
	}
	public void setCoupon(int coupon) {
		this.coupon = coupon;
		this.Calcdiscount(probe);
	}
	//Calculate
	
	public void Calcdiscount(date date) {
		int  month = proend.takeMonth();
		if( ( month >= 8 ) && (month <= 10) ) 
			this.coupon=30;
		else if ( (month >= 6) && (month <= 7))
			this.coupon=15;
		else
			this.coupon=0;
	}
	
	public String toString() {
		return 	"Reference code: " + this.getCref() + "\nBrand: " + this.getBrand() + "\nModel: " + this.getModel() + "\nMaterial: " + this.getMaterial() + "\nColour: " + this.getColour() + "\nSize: " + this.getSize() + "\nYour email: " + this.getEmail() + "\nYour telephone: " + this.getTelp() + "\nOutlet: " + this.outlet + 
				"\nType sole: " + this.getT_Sole() + "\nType profile: " + this.getT_Profile() + "\nYou get a discount of: " + this.coupon + "\n";
	}
	public String toString(String cref) {
		String cad = "";
		
		cad=cad + (super.getCref() + "\n");
		
		return cad;
	}
	
	
}
