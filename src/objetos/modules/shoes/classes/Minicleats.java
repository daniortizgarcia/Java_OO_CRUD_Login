package objetos.modules.shoes.classes;

import objetos.classes.*;

public class Minicleats extends Shoes_f{
		
	private String shape_mt;
	
	public Minicleats (String cref, String brand, String model, String material, String colour, int size, date darri, String email, String telp, String shape_mt) {
		super(cref, brand, model, material, colour, size, darri, email, telp);
		this.shape_mt=shape_mt;
	}
	
	public Minicleats() {
		super();
	}
	public Minicleats(String cref) {
		super(cref);
	}
	
	//getter
	
	public String getShape_mt() {
		return this.shape_mt;
	}
	
	//setter
	
	public void setShape_mt(String shape_mt) {
		this.shape_mt=shape_mt;
	}
	
	public String toString() {
		return 	"Reference code: " + this.getCref() + "\nBrand: " + this.getBrand() + "\nModel: " + this.getModel() + "\nMaterial: " + this.getMaterial() + "\nColour: " + this.getColour() + "\nSize: " + this.getSize() + "\nYour email: " + this.getEmail() + "\nYour telephone: " + this.getTelp() + "\nOutlet: " + this.outlet + 
				"\nshape minicleats: " +  this.getShape_mt() + "\n";
	}
	
	public String toString(String cref) {
		String cad = "";
		
		cad=cad + (super.getCref() + "\n");
		
		return cad;
	}

}
