package objetos;

import objetos.modules.shoes.classes.*;
import objetos.modules.shoes.utils.CRUD.*;
import objetos.utils.menu;

public class function_login {

	public static boolean l_adminf(Flat f) {
		int choose = 0;
		boolean continuar = false;
		String[] tipoa = { "Create", "Change", "Print", "Delete", "Order", "Come back", "Exit" };

		do {

			choose = menu.validaboton(tipoa, "Select the option you prefer", "Flat");
			switch (choose) {
			case 0:
				function_create.c_flat(f);
				break;
			case 1:
				function_update.u_flat(f);
				break;
			case 2:
				function_read.r_flat();
				break;
			case 3:
				function_delete.d_flat();
				break;
			case 4:
				function_order.o_flat();
				break;
			case 5:
				
				break;
			default:
				continuar = true;
				break;
			}
		} while (choose < 5);
		return continuar;
		
	}

	public static boolean l_adminm(Minicleats m) {
		int choose = 0;
		boolean continuar = false;
		String[] tipoa = { "Create", "Change", "Print", "Delete", "Order", "Come back", "Exit" };

		do {
			choose = menu.validaboton(tipoa, "Select the option you prefer", "Minicleats");
			switch (choose) {
			case 0:
				function_create.c_mcleats(m);
				break;
			case 1:
				function_update.u_mcleats(m);
				break;
			case 2:
				function_read.r_mcleats();
				break;
			case 3:
				function_delete.d_mcleats();
				break;
			case 4:
				function_order.o_mcleats();
				break;
			case 5:

				break;
			default:
				continuar = true;
				break;
			}
		} while (choose < 5);
		return continuar;
	}

	public static boolean l_adminc(Cleats c) {
		int choose = 0;
		boolean continuar = false;
		String[] tipoa = { "Create", "Change", "Print", "Delete", "Order", "Come back", "Exit" };

		do {
			choose = menu.validaboton(tipoa, "Select the option you prefer", "Cleats");
			switch (choose) {
			case 0:
				function_create.c_cleats(c);
				break;
			case 1:
				function_update.u_cleats(c);
				break;
			case 2:
				function_read.r_cleats();
				break;
			case 3:
				function_delete.d_cleats();
				break;
			case 4:
				function_order.o_cleats();
				break;
			case 5:

				break;
			default:
				continuar = true;
				break;
			}
		} while (choose < 5);
		return continuar;
	}

	public static boolean l_receptionf(Flat f) {
		int choose = 0;
		boolean continuar = false;
		String[] tipor = { "Change", "Print", "Order", "Come back", "Exit" };
		do {
			choose = menu.validaboton(tipor, "Select the option you prefer", "Flat");
			switch (choose) {

			case 0:
				function_update.u_flat(f);
				break;
			case 1:
				function_read.r_flat();
				break;
			case 2:
				function_order.o_flat();
				break;
			case 3:

				break;
			default:
				continuar = true;
				break;
			}
		} while (choose < 3);
		return continuar;
	}

	public static boolean l_receptionm(Minicleats m) {
		int choose = 0;
		boolean continuar = false;
		String[] tipor = { "Change", "Print", "Order", "Come back", "Exit" };

		do {
			choose = menu.validaboton(tipor, "Select the option you prefer", "Minicleats");
			switch (choose) {
			case 0:
				function_update.u_mcleats(m);
				break;
			case 1:
				function_read.r_mcleats();
				break;
			case 2:
				function_order.o_mcleats();
				break;
			case 3:

				break;
			default:
				continuar = true;
				break;
			}
		} while (choose < 3);
		return continuar;
	}

	public static boolean l_receptionc(Cleats c) {
		int choose = 0;
		boolean continuar = false;
		String[] tipor = { "Change", "Print", "Order", "Come back", "Exit" };
		
		do {
			choose=menu.validaboton(tipor, "Select the option you prefer", "Cleats");
			switch(choose) {
				case 0:
						function_update.u_cleats(c);
						break;
				case 1:
						function_read.r_cleats();
						break;
				case 2:
						function_order.o_cleats();
						break;
				case 3:
						
						break;
				default:
						continuar = true;
						break;
			}
		}while(choose < 3);
		return continuar;
	}
	
	

	public static boolean l_userf() {
		int choose = 0;
		boolean continuar = false;
		String[] tipou = { "Print", "Order", "Come back", "Exit" };
		
		do {
			choose=menu.validaboton(tipou, "Select the option you prefer", "Flat");
			switch(choose) {
				
				case 0:
						function_read.r_flat();
						break;
				case 1:
						function_order.o_flat();
						break;
				case 2:
					
						break;
				default:
						continuar = true;
						break;
			}
		}while(choose < 2);
		return continuar;
	}

	public static boolean l_userm() {
		int choose = 0;
		boolean continuar = false;
		String[] tipou = { "Print", "Order", "Come back", "Exit" };
		
		do {
			choose=menu.validaboton(tipou, "Select the option you prefer", "Minicleats");
			switch(choose) {
				case 0:
						function_read.r_mcleats();
						break;
				case 1: 
						function_order.o_mcleats();
						break;
				case 2:
					
						break;
				default:
						continuar = true;
						break;
			}
		}while(choose < 2);
		return continuar;
	}

	public static boolean l_userc() {
		int choose = 0;
		boolean continuar = false;
		String[] tipou = { "Print", "Order", "Come back", "Exit" };
		
		do {
			choose=menu.validaboton(tipou, "Select the option you prefer", "Cleats");
			switch(choose) {
				case 0:
						function_read.r_cleats();
						break;
				case 1:
						function_order.o_cleats();
						break;
				case 2:
						
						break;
				default:
						continuar = true;
						break;
			}
		}while(choose < 2);
		return continuar;
	}
	
	

}
