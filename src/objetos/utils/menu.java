package objetos.utils;


import javax.swing.JOptionPane;

public class menu {

	//Demane combobox
	public static String combobox(String[] tipo, String informacion, String titulo) {
		Object seleccion = null;
		
		seleccion=JOptionPane.showInputDialog(
					null, 
					informacion, 
					titulo, 
					JOptionPane.QUESTION_MESSAGE,
					null, tipo, tipo[0]);
		if(seleccion == null) {
			JOptionPane.showMessageDialog(null,"Saliendo de la aplicación","Saliendo ...",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
		}
			return seleccion.toString();
	}
	
	//Demane viñeta con varios botones
	public static int validaboton(String[] tipo, String informacion, String titulo) {
		int eleccion = 0;

		eleccion = JOptionPane.showOptionDialog(null,
								informacion, titulo, 0,
								JOptionPane.QUESTION_MESSAGE,null, tipo, tipo[0]);
		if(eleccion == -1) {
			JOptionPane.showMessageDialog(null,"Saliendo de la aplicación","Saliendo ...",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
		}

		return eleccion;
		
	}

	//Funcion para repetir
	public static void repetir(String[] tipo1, int[] continuar) {
		//La posicion vector[0] guarda la opcion de repetir la operacion
		//La posicion vector[1] guarda la opcion de continuar o salir del programa
		continuar[0] = JOptionPane.showOptionDialog(null,
				"Que desea hacer?", "continuar", 0,
				JOptionPane.QUESTION_MESSAGE, null, tipo1,
				tipo1[0]);
		switch (continuar[0]) {
		
		case 1:// Aço es quan es pulsa repetir, continuar[1]=1 es per que no acabe el bucle global de la calculadora
			continuar[1] = 1;
			break;
		case 2: 
			continuar[1] = 0;
			break;
		default:
			continuar[1] = 0;
		}
	}
	
	//Preguntar confirmacion
	public static int confirmar(String descripcion) {
		int confirmar = 0;
		confirmar = JOptionPane.showConfirmDialog(null,descripcion);
		if(confirmar == -1) {
			JOptionPane.showMessageDialog(null,"Saliendo de la aplicación","Saliendo ...",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
		}
		else if (JOptionPane.CANCEL_OPTION == confirmar) {
			JOptionPane.showMessageDialog(null,"Cancelado","Cancelado ...",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
		}
		
		System.out.println(confirmar);
		return confirmar;
	}
}
