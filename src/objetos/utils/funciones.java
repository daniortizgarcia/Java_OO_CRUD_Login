package objetos.utils;

import javax.swing.*;

public class funciones {

	//valida_int
	public static int validaint(String informacion, String titulo) {
		int num_int = 0;
		String s = "";
		boolean correcto = false;
		
		do{
			try{
				s=JOptionPane.showInputDialog(null, informacion,titulo,JOptionPane.QUESTION_MESSAGE);
				if(s==null){
					JOptionPane.showMessageDialog(null, "Saliendo de la aplicación","Saliendo",JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
				}else {
					num_int=Integer.parseInt(s);
					correcto=true;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "No has introducido un num int", "Error",JOptionPane.ERROR_MESSAGE);
				correcto=false;
			}
		}while(correcto==false);
		System.out.println(num_int);
		return(num_int);
	}
	
	//validaString
	public static String validastring(String informacion, String titulo) {
		String s="";
		boolean correcto=true;

		do{
			try{
				s=JOptionPane.showInputDialog(null, informacion,titulo,JOptionPane.QUESTION_MESSAGE);
				correcto=true;
				if (s==null){
					JOptionPane.showMessageDialog(null, "Saliendo de la aplicación","Saliendo",JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
				}
				if(s.equals("")){
					JOptionPane.showMessageDialog(null, "Error de introducción de datos","Error",JOptionPane.ERROR_MESSAGE);
					correcto=false;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "No has introducido una cadena", "Error",JOptionPane.ERROR_MESSAGE);
				correcto=false;
			}
		}while(correcto==false);
		System.out.println(s);
		return s;
	}
	
	//Valida float
	public static float validafloat(String informacion, String titulo) {
		float num_f = 0.0f;
		String s;
		boolean correcto=true;

		do{
			try{
				s=JOptionPane.showInputDialog(null, informacion,titulo,JOptionPane.QUESTION_MESSAGE);
				if(s==null){
					JOptionPane.showMessageDialog(null, "Introduce un numero valido","Saliendo",JOptionPane.INFORMATION_MESSAGE);
				}else {
					num_f=Float.parseFloat(s);
					correcto=true;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "No has introducido un num float", "Error",JOptionPane.ERROR_MESSAGE);
				correcto=false;
			}
		}while(correcto==false);	
		System.out.println(num_f);
		return (num_f);
	}
	
	//valida char
	public static char validachar(String informacion, String titulo) {
		char c = 0;
		String s;
		boolean correcto=true;

		do{
			try{
				s=JOptionPane.showInputDialog(null, informacion,titulo,JOptionPane.QUESTION_MESSAGE);
				if(s==null){
					JOptionPane.showMessageDialog(null, "Saliendo de la aplicación","Saliendo",JOptionPane.INFORMATION_MESSAGE);
					System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecución
				}else {
					c=s.charAt(0);
					correcto=true;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "No has introducido una letra", "Error",JOptionPane.ERROR_MESSAGE);
				correcto=false;
			}
		}while(correcto==false);
		System.out.println(c);
		return c;
	}
	
	//Girar numeros
	public static int giranum(int num) {
		int res = 0; int num_inv = 0;
		String aux1 = "";
		
		while(num > 0) {
			res=num%10;
			num=num/10;
			aux1=aux1 + (Integer.toString(res));
		}//end_while
		num_inv=Integer.parseInt(aux1);

		return(num_inv);
	}
	

}
