package objetos.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class validate {
	
	private static final String requeriment_vrefc = "^[0-9]{5}[-][a-z]{1}$";
	private static final String requeriment_email = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String requeriment_telp = "^[+34]{3}[ ][0-9]{9}$";
	private static final String requeriment_date = "[0-9]{2}[/][0-9]{2}[/][0-9]{4}";
	
	//Valide reference code
		public static boolean v_refc(String cref) {
			Pattern pattern=Pattern.compile(requeriment_vrefc);
			Matcher matcher = pattern.matcher(cref);
			
			if(matcher.matches())
				return true;
			else
				return false;
		}
	//Valide email
	public static boolean v_email(String email) {
		Pattern pattern=Pattern.compile(requeriment_email);
		Matcher matcher = pattern.matcher(email);
		
		if(matcher.matches())
			return true;
		else
			return false;
	}
	
	//Valida telephone
	public static boolean v_telp(String telp) {
		Pattern pattern=Pattern.compile(requeriment_telp);
		Matcher matcher = pattern.matcher(telp);
		
		if(matcher.matches())
			return true;
		else
			return false;
	}
	
	//valide date
	public static boolean v_date(String date) {
		Pattern pattern=Pattern.compile(requeriment_date);
		Matcher matcher = pattern.matcher(date);
		
		if(matcher.matches())
			return true;
		else
			return false;
	}

	
}
